package com.example.angeeeld.rxkotlinandretrofit.Views.Main

import com.example.angeeeld.rxkotlinandretrofit.Protocol.Entities.Kotlin.GitHubUserKotlin

/**
 * Created by angeeeld on 1/25/18.
 */
interface MainContract {

  interface View {

    fun onGetUsersStart()
    fun showUsers(users: List<GitHubUserKotlin>)
    fun onGetUsersFail()

    fun onGetUserStart()
    fun showUser(user: GitHubUserKotlin)
    fun onGetUserFail()

  }

  interface Presenter {

    fun getUsers()
    fun getUser(username: String)

  }

}