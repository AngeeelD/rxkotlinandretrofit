package com.example.angeeeld.rxkotlinandretrofit.Views.Main

import android.content.Context
import com.example.angeeeld.rxkotlinandretrofit.Protocol.Clients.GitHubClient
import com.example.angeeeld.rxkotlinandretrofit.Protocol.Core.GithubServiceGenerator
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by angeeeld on 1/25/18.
 */
class MainPresenter(private val view: MainContract.View,
                    private val context: Context) : MainContract.Presenter {

  //region MainContract.Presenter
  override fun getUsers() {
    view.onGetUsersStart()
    GithubServiceGenerator
        .createService(GitHubClient::class.java)
        .getUsersForKotlin()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          view.showUsers(it)
        }, {
          view.onGetUsersFail()
        })
  }

  override fun getUser(username: String) {
    view.onGetUserStart()
    GithubServiceGenerator
        .createService(GitHubClient::class.java)
        .getUserForKotlin(username)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          view.showUser(it)
        }, {
          view.onGetUserFail()
        })
  }
  //endregion

}