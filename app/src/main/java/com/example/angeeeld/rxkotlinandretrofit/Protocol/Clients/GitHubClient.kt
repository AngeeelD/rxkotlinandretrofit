package com.example.angeeeld.rxkotlinandretrofit.Protocol.Clients

import com.example.angeeeld.rxkotlinandretrofit.Protocol.Entities.Java.GitHubUser
import com.example.angeeeld.rxkotlinandretrofit.Protocol.Entities.Kotlin.GitHubUserKotlin
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by angeeeld on 1/25/18.
 */
interface GitHubClient {

  @GET("/users")
  fun getUsersForJava(): Observable<List<GitHubUser>>

  @GET("/users")
  fun getUsersForKotlin(): Observable<List<GitHubUserKotlin>>

  @GET("/users/{user}")
  fun getUserForJava(@Path("user") username: String): Observable<GitHubUser>

  @GET("/users/{user}")
  fun getUserForKotlin(@Path("user") username: String): Observable<GitHubUserKotlin>

}