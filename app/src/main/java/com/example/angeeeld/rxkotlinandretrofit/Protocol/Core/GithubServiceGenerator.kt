package com.example.angeeeld.rxkotlinandretrofit.Protocol.Core

import com.example.angeeeld.rxkotlinandretrofit.BuildConfig
import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by angeeeld on 1/24/18.
 */
object GithubServiceGenerator {

  var apiBaseUrl = BuildConfig.GITHUB_URL_BASE

  private val retrofit: Retrofit? = null

  private val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

  private var builder: Retrofit.Builder = Retrofit.Builder()
      .addConverterFactory(GsonConverterFactory.create(
          GsonBuilder()
              .setDateFormat("YYYY-MM-DDTHH:MM:SSZ")
              .create()
      ))
      .addCallAdapterFactory(rxAdapter)
      .baseUrl(apiBaseUrl)

  private val httpClient = OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

  fun changeApiBaseUrl(newApiBaseUrl: String) {
    apiBaseUrl = newApiBaseUrl

    builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(
            GsonBuilder()
                .setDateFormat("YYYY-MM-DDTHH:MM:SSZ")
                .create()
        ))
        .baseUrl(apiBaseUrl)
  }

  fun <S> createService(serviceClass: Class<S>): S {
    val retrofit = builder.client(httpClient.build()).build()
    return retrofit.create(serviceClass)
  }

}