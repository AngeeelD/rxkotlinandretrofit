package com.example.angeeeld.rxkotlinandretrofit.Views.Main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.angeeeld.rxkotlinandretrofit.Protocol.Entities.Kotlin.GitHubUserKotlin
import com.example.angeeeld.rxkotlinandretrofit.R

class MainActivity : AppCompatActivity(), MainContract.View {

  // val presenter = MainPresenter()
  // val presenter : MainPresenter = MainPresenter()
  var presenter: MainPresenter? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    presenter = MainPresenter(this, this)
  }

  //region MainContract.View
  override fun onGetUsersStart() {
    //animacion de carga
  }

  override fun showUsers(users: List<GitHubUserKotlin>) {
    //mostrar usuarios
  }

  override fun onGetUsersFail() {
    //mostrar error
  }

  override fun onGetUserStart() {
    //show Loading on get user
  }

  override fun showUser(user: GitHubUserKotlin) {
    //show user information
  }

  override fun onGetUserFail() {
    //show error on get single user
  }
  //endregion

}
